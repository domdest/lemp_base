include ../.env
export

prod-up :
	$(info Make: Starting prod environment containers.)
	docker network create proxy.$(PROD_DOMAIN_NAME)
	DOMAIN_NAME=$(PROD_DOMAIN_NAME) ADMIN_EMAIL=$(ADMIN_EMAIL) docker-compose -f docker-compose.yml up -d
prod-debug-up :
	$(info Make: Starting prod environment containers WITH DEBUG.)
	docker network create proxy.$(PROD_DOMAIN_NAME)
	DOMAIN_NAME=$(PROD_DOMAIN_NAME) ADMIN_EMAIL=$(ADMIN_EMAIL) docker-compose -f docker-compose.yml up
prod-down :
	$(info Make: Stopping prod environment containers.)
	DOMAIN_NAME=$(PROD_DOMAIN_NAME) ADMIN_EMAIL=$(ADMIN_EMAIL) docker-compose -f docker-compose.yml down
	docker network prune -f
dev-up :
	$(info Make: Starting dev environment containers.)
	DOMAIN_NAME=$(DEV_DOMAIN_NAME) ADMIN_EMAIL=$(ADMIN_EMAIL) docker-compose -f docker-compose.dev.yml up
dev-down :
	$(info Make: Stopping dev environment containers.)
	DOMAIN_NAME=$(DEV_DOMAIN_NAME) ADMIN_EMAIL=$(ADMIN_EMAIL) docker-compose -f docker-compose.dev.yml down
	docker network prune -f