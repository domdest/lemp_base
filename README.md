Change value of ENTER_PROD_DOMAIN_NAME in makefile to your domain, e.g. "example.com"

Change value of ENTER_DEV_DOMAIN_NAME in makefile to your domain, e.g. "example.test"

Change value of ENTER_ADMIN_EMAIL in makefile to the site administrator's email.

Change value of ENTER_PROD_DOMAIN_NAME in .gitlab-ci.yml to your prod domain, e.g. "example.com"

Change value of ENTER_PROJECT_NAME in .gitlab-ci.yml to your Gitlab project name

Change value of ENTER_DB_NAME in build/mysql/.env to your desired database name

Change value of ENTER_DB_ROOT_PASSWORD in build/mysql/.env to your desired root password

Change value of ENTER_MYSQL_USER in build/mysql/.env to your desired user name.

Change value of ENTER_MYSQL_PASSWORD in build/mysql/.env to the desired password for your user.

Change value of ENTER_GITLAB_RUNNER_PASSWORD in build/sh/gcp_startup.sh to the desired password for your Gitlab job runner.

Change value of ENTER_PROD_DOMAIN_NAME in docker-compose.yml to your domain.

Change value of ENTER_DEV_DOMAIN_NAME in docker-compose.dev.yml to your domain.